terraform {
  backend "s3" {
    bucket = "ismaeeltestingbackendbucket"
    key    = "dev/terraform.state"
    region = "us-east-2"
  }
}

provider "aws" {
  region = "us-east-2"
  shared_credentials_file = ""
  profile = "default"
}

resource "aws_instance" "webserver" {
 ami = "ami-00399ec92321828f5"
 instance_type = "t2.micro"

 key_name = "ismaeeluseast2kp"

 tags = {
 Name = "ismaeel_testing_stages_in_gitlab-testing"
 }



}
